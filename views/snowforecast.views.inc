<?php
/**
 * @file
 * Views implementation functions.
 */

/**
 * Implementation of hook_views_handlers().
 *
 * @todo
 *   Add handlers and formatters to handle conversion from celsius and metric values.
 */
function snowforecast_views_handlers() {
  $path = drupal_get_path('module', 'snowforecast') .'/views';
  return array(
    'handlers' => array(
      'snowforecast_views_handler_filter_period' => array(
        'parent' => 'views_handler_filter_in_operator',
        'path' => $path,
      ),
      'snowforecast_views_handler_filter_elevation' => array(
        'parent' => 'views_handler_filter_in_operator',
        'path' => $path,
      ),
      'snowforecast_views_handler_relationship_data' => array(
        'parent' => 'views_handler_relationship',
        'path' => $path,
      ),
      'snowforecast_views_handler_relationship_period' => array(
        'parent' => 'views_handler_relationship',
        'path' => $path,
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 */
function snowforecast_views_data() {
  return array(
    'snowforecast_periods' => array(
      'table' => array(
        'group' => t('Weather period'),
        'base' => array(
          'field' => 'pid',
          'title' => t('Weather periods'),
          'help' => t('Weather periods from www.snow-forecast.com for all defined locations.'),
        ),
      ),
      'pid' => array(
        'title' => t('Pid'),
        'help' => t('The period ID for the period.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'relationship' => array(
          'title' => t('Data'),
          'help' => t('The data which belongs to a period.'),
          'base' => 'snowforecast_data',
          'field' => 'pid',
          'handler' => 'snowforecast_views_handler_relationship_data',
        ),
      ),
      'name' => array(
        'title' => t('Name'),
        'help' => t('The period name.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'filter' => array(
          'handler' => 'snowforecast_views_handler_filter_period',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'start' => array(
        'title' => t('Start date'),
        'help' => t('The date the period starts.'),
        'field' => array(
          'handler' => 'views_handler_field_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
      ),
      'end' => array(
        'title' => t('End date'),
        'help' => t('The date the period ends.'),
        'field' => array(
          'handler' => 'views_handler_field_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
      ),
      'freeze_level' => array(
        'title' => t('Freeze level'),
        'help' => t('The freeze level (in meters) for the period.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
    ),
    'snowforecast_issues' => array(
      'table' => array(
        'group' => t('Weather period'),
        'join' => array(
          'snowforecast_periods' => array(
            'left_field' => 'iid',
            'field' => 'iid',
          ),
        ),
      ),
      'iid' => array(
        'title' => t('Iid'),
        'help' => t('The issue ID this period belongs to.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ), 
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'timestamp' => array(
        'title' => t('Issue date'),
        'help' => t('The date when this issue was created.'),
        'field' => array(
          'handler' => 'views_handler_field_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),        
      ),
    ),
    'snowforecast_locations' => array(
      'table' => array(
        'group' => t('Weather period'),
        'join' => array(
          'snowforecast_locations' => array(
            'left_table' => 'snowforecast_issues',
            'left_field' => 'lid',
            'field' => 'lid',
          ),
        ),
      ),
      'lid' => array(
        'title' => t('Lid'),
        'help' => t('The location ID this period belongs to.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ), 
      ),
      'uri' => array(
        'title' => t('Location URI'),
        'help' => t('The URI of the location this period belongs to'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
    ),
    'snowforecast_data' => array(
      'table' => array(
        'group' => t('Weather data'),
        'base' => array(
          // This isn't the primary key. Why must we set this
          // to 'pid' to get the relationship working?
          'field' => 'pid',
          'title' => t('Weather data'),
          'help' => t('Weather data from www.snow-forecast.com for all defined locations.'),
        ),
      ),
      'did' => array(
        'title' => t('Did'),
        'help' => t('The data ID for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'elevation' => array(
        'title' => t('Elevation'),
        'help' => t('The elevation for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'filter' => array(
          'handler' => 'snowforecast_views_handler_filter_elevation',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'argument' => array(
		  'handler' => 'views_handler_argument',
        ),
      ),
      'phrase' => array(
        'title' => t('Summary phrase'),
        'help' => t('The summary phrase for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'icon' => array(
        'title' => t('Summary icon'),
        'help' => t('The summary icon for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
      'snow_amount' => array(
        'title' => t('Snow amount'),
        'help' => t('The snow amount (in millimeters) for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'rain_amount' => array(
        'title' => t('Rain amount'),
        'help' => t('The rain amount (in millimeters) for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'temperature_max' => array(
        'title' => t('Max temperature'),
        'help' => t('The maximum temperature (in celcius) for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'temperature_min' => array(
        'title' => t('Min temperature'),
        'help' => t('The minimum temperature (in celcius) for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'wind_speed' => array(
        'title' => t('Wind speed'),
        'help' => t('The wind speed (in meters/second) for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'wind_chill' => array(
        'title' => t('Wind chill'),
        'help' => t('The wind chill (in celcius) for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'wind_phrase' => array(
        'title' => t('Wind summary phrase'),
        'help' => t('The wind summary phrase for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
      ),
      'wind_icon' => array(
        'title' => t('Wind summary icon'),
        'help' => t('The wind summary icon for the weather data.'),
        'field' => array(
          'handler' => 'views_handler_field',
        ),
      ),
    ),
  );
}
