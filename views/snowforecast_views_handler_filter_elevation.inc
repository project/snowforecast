<?php

class snowforecast_views_handler_filter_elevation extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Elevations');
      $this->value_options = snowforecast_get_names('elevation');
    }
  }
}
