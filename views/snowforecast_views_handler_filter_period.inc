<?php

class snowforecast_views_handler_filter_period extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Periods');
      $this->value_options = snowforecast_get_names('period');
    }
  }
}
