<?php
/**
 * @file
 * This file holds all neccessary functions to request and parse
 * forecast information from www.snow-forecast.com.
 */

/**
 * Request a new forecast issue for a given location.
 *
 * @param $location
 *   The location to request an issue for.
 *
 * @return
 *   A structured issue array.
 */
function snowforecast_issue_request($location) {
  $url = strtr(SNOWFORECAST_URL, array('%s' => $location->uri));
  $response = drupal_http_request($url);

  if ($response->code != 200) {
    watchdog('snowforecast', 'There was an error in the request.');
    return FALSE;
  }
  $issue = array();

  if ($response->data) {
    $issue = _snowforecast_issue_map($response->data, $location);
  }
  return $issue;
}

/**
 * This function parses the retieved XML feed.
 *
 * This is the interface between the XML feed and our database. This
 * function makes some assumptions about the current schema.
 *
 * We store the data in a more readable way than the original
 * XML feed from Snow-forecast.com.
 */
function _snowforecast_issue_map($data, $location) {
  // Convert the raw XML to a usable SimpleXML object.
  $object = new SimpleXMLElement($data);

  // Initiate our structured issue array.
  $issue = array();
  $issue['lid'] = $location->lid;
  $issue['timestamp'] = strtotime($object->_gmtissued);
  $issue['#periods'] = array();

  $i = 0;
  foreach ($object->periods->period as $period) {
    $issue['#periods'][$i]['name']         = snowforecast_value($period->_plcname);
    $issue['#periods'][$i]['start']        = snowforecast_value($period->_ptstampstart);
    $issue['#periods'][$i]['end']          = snowforecast_value($period->_ptstampend);
    $issue['#periods'][$i]['freeze_level'] = snowforecast_value($period->_pflevel);

    $elevations = snowforecast_get_names('elevation');

    foreach ($elevations as $key => $name) {
      $issue['#periods'][$i]['#data'][$key]['elevation']       = $key;
      $issue['#periods'][$i]['#data'][$key]['phrase']          = snowforecast_value($period->{$key}->_pphrase);
      $issue['#periods'][$i]['#data'][$key]['icon']            = snowforecast_value($period->{$key}->_psymbol);
      $issue['#periods'][$i]['#data'][$key]['snow_amount']     = snowforecast_value($period->{$key}->_psnow);
      $issue['#periods'][$i]['#data'][$key]['rain_amount']     = snowforecast_value($period->{$key}->_pprec);
      $issue['#periods'][$i]['#data'][$key]['temperature_max'] = snowforecast_value($period->{$key}->_pmax);
      $issue['#periods'][$i]['#data'][$key]['temperature_min'] = snowforecast_value($period->{$key}->_pmin);
      $issue['#periods'][$i]['#data'][$key]['wind_speed']      = snowforecast_value($period->{$key}->_pwind);
      $issue['#periods'][$i]['#data'][$key]['wind_chill']      = snowforecast_value($period->{$key}->_pminchill);
      $issue['#periods'][$i]['#data'][$key]['wind_phrase']     = snowforecast_value($period->{$key}->_pwphrase);
      $issue['#periods'][$i]['#data'][$key]['wind_icon']       = snowforecast_value($period->{$key}->_pwsymbol);
    }
    $i++;
  }
  return $issue;
}

/**
 * Check values.
 */ 
function snowforecast_value($object) {
  $string = (string) $object;

  if ($string == '-') {
    return 0;
  }
  if (substr($string, -4) == '.gif') {
    $string = substr($string, 0, -4);
  }
  return filter_xss($string, array());
}
