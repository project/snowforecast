<?php

/**
 * Page callback for the location admin page.
 */
function snowforecast_locations_admin_page() {
  $output = '';

  // Prepare the table.
  $header = array(
    t('Location'),
    t('URI'),
    t('Last updated'),
    array('data' => t('Operations'), 'colspan' => 3),
  );
  $rows = array();

  $result = db_query('SELECT l.*, i.timestamp FROM {snowforecast_locations} l LEFT JOIN {snowforecast_issues} i ON i.lid = l.lid GROUP BY l.lid ORDER BY i.timestamp DESC');
  while ($location = db_fetch_object($result)) {
    $path = 'admin/content/snowforecast/locations/'. $location->lid;
    $rows[] = array(
      array('data' => $location->title),
      array('data' => $location->uri),
      array('data' => format_date($location->timestamp, 'large')),
      array('data' => l(t('Update'), $path .'/update')),
      array('data' => l(t('Edit'), $path .'/edit')),
      array('data' => l(t('Delete'), $path .'/delete')),
    );
  }
  $output .= theme('table', $header, $rows);
  return $output;
}

/**
 * Page callback for the location update page.
 *
 * @see snowforecast_issue_update().
 */
function snowforecast_location_update_page($location) {
  snowforecast_issue_update($location);
  drupal_goto('admin/content/snowforecast/locations');
}

/**
 * Form callback for the location form.
 */
function snowforecast_location_form(&$form_state = array(), $location = array()) {
  $form = array();

  $form['location']['lid'] = array(
    '#type' => 'value',
    '#default_value' => $location->lid,
  );
  $form['location']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Location title'),
    '#description' => t('This should be the human readable title of the location.'),
    '#required' => TRUE,
    '#default_value' => $location->title,
  );
  $form['location']['uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Location URI'),
    '#description' => t('This should be the machine readable text which also represents the URI in this URL: @url-placeholder. <a href="!url-location">All available locations can be found here</a>.', array('@url-placeholder' => 'http://www.snow-forecast.com/resorts/<name>/feed.xml', '!url-location' => 'http://www.snow-forecast.com/countries')),
    '#required' => TRUE,
    '#default_value' => $location->uri,
  );
  $form['location']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#description' => t('This isn\'t really used anywhere yet.'),
    '#delta' => 10,
    '#default_value' => isset($location->weight) ? $location->weight : 0,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Submit callback for the location form.
 */
function snowforecast_location_form_submit($form, &$form_state) {
  if ($form_state['values']['submit']) {
    snowforecast_location_save($form_state['values']);
  }
  $form_state['redirect'] = 'admin/content/snowforecast/locations';
}

/**
 * Form callback for location delete form.
 */
function snowforecast_location_delete_form($form_state, $location) {
  $form = array();
  $form['lid'] = array(
    '#type' => 'value',
    '#value' => $location->lid,
  );
  return confirm_form(
    $form,
    t('Are you sure you delete %title', array('%title' => $location->title)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/content/snowforecast/locations',
    t('This action will delete all associated forecast issues and cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit callback for location delete form.
 */
function snowforecast_location_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    snowforecast_location_delete($form_state['values']['lid']);
    $form_state['redirect'] = 'admin/content/snowforecast/locations';
  }
}
